# Zack's Lua Multimark
This mod enables you to use the built in mark/recall spells and enchantments to save a number of locations.

The mod uses the same ESP changes as in [Multiple Teleport Marking](https://www.nexusmods.com/morrowind/mods/44825), replacing the mark and recall effects with "Greater Mark" and "Greater Recall"

In the future, this won't be necessary, but only for the 0.49+ version.

By default, you can save one marked location per 5 mysticism. You can adjust this in the settings, and if desired, set a base count, that does not take mysticism into account.

When you mark, you will automatically add the mark to the list of saved locations. If you don't have any available locations, you will be prompted to replace one.

When you recall, you will see the menu in the screenshot, you will have the option to select a destination, and you can rename them as well.

This menu is fully controller compatible.

There will be a menu in the top left with a list of keys you can press.

To enable the mod, please make sure to enable both the omwaddon and omwscripts file in the OpenMW launcher.

Those who are following you will also be teleported, unless you disable this.

Credit to the above modder, Marcel Hesselbarth, who made the original multiple marking mod.

With this mod not relying on the original engine's scripting, it's able to provide features such as renaming, being able to mark truly anywhere, and other enhancements.

If you use the original engine and not OpenMW, you can download [MWSE Multimark](https://www.nexusmods.com/morrowind/mods/47065).

#### Credits

Author: ZackHasaCat

#### Installation

1. Download the mod from [this URL](https://gitlab.com/api/v4/projects/zackhasacat%2Fzacks-lua-multimark/jobs/artifacts/master/raw/zacks-lua-multimark.zip?job=make)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\MultiMark

        # Linux
        /home/username/games/OpenMWMods/MultiMark

        # macOS
        /Users/username/games/OpenMWMods/MultiMark

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\LuaMultiMark"`)
1. Add `content=LuaMultiMark.omwaddon` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/zacks-lua-multimark/-/issues)
* Email `zack@iwsao.com`
* Contact the author on Discord: `@ZackHasaCat`
